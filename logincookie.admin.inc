<?php

function logincookie_delete_form($form_status, $cookie){
  //drupal_set_message('<pre>'. print_r($cookie, 1 ) . '</pre>');
  $form = array();
  $cookie['setunset'] = arg(4);
  $form['cookie'] = array(
    '#type' => 'value',
    '#value' => $cookie,
  );
  return confirm_form($form,
    t('Are you sure you want to delete'),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/logincookie',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}
function logincookie_delete_form_submit($form, &$form_state) {
  $logincookie_default = array(
    'set'   => array(),
    'unset' => array(),
  );
  $cookie = $form_state['values']['cookie'];

  //drupal_set_message('<pre>'. print_r($form_state, 1 ) . '</pre>');
  //drupal_set_message('<pre>'. print_r($cookie, 1 ) . '</pre>');
  $cookies = variable_get('logincookie_'. $cookie['when'], $logincookie_default);
  //drupal_set_message('<pre>'. print_r($cookies, 1 ) . '</pre>');
  if (isset($cookies[$cookie['setunset']][$cookie['name']]) )  {
    unset($cookies[$cookie['setunset']][$cookie['name']]);
  }
  //drupal_set_message('<pre>'. print_r($cookies, 1 ) . '</pre>');
  variable_set('logincookie_'. $cookie['when'], $cookies);
  $form_state['redirect'] = 'admin/settings/logincookie';
}
function logincookie_set_form($form_status, $cookie){
  //drupal_set_message('<pre>'. print_r($cookie, 1 ) . '</pre>');
  $form = array();
  $form['when'] = array(
    '#type'     => 'select',
    '#title'    => t('Cookie action'),
    '#description' => t('Select <em>when</em> to set cookie. Ex: selecting "login" will set this cookie when user logs in the site.'),
    '#options'  => array(
      '0' => 'Choose action',
      'login' => 'Login',
      'logout' => 'Logout',
    ),
    '#default_value' => isset($cookie['when']) ? $cookie['when'] : '0',
  );
  if (isset($cookie)) {
    $form['old-cookie'] = array(
      '#type' => 'value',
      '#value' => $cookie,
    );
  }
  $form['name'] = array(
    '#type'       => 'textfield',
    '#title'      => t('Cookie Name'),
    '#description'=> t('The name of the cookie.'),
    '#default_value' => isset($cookie['name']) ? $cookie['name'] : '',
  );
  $form['value'] = array(
    '#type'       => 'textfield',
    '#title'      => t('Cookie value'),
    '#description'=> t('The value of the cookie. This value is stored on the clients computer; do not store sensitive information.'),
    '#default_value' => isset($cookie['value']) ? $cookie['value'] : '',
  );
  $form['expire'] = array(
    '#type'       => 'textfield',
    '#title'      => 'Cookie expire',
    '#description'=> 'Time in seconds when cookie will expire after being set.',
    '#default_value' => isset($cookie['expire']) ? $cookie['expire'] : 0,
  );
  $form['path'] = array(
    '#type'       => 'textfield',
    '#title'      => t('Cookie path'),
    '#description'=> t("The path on the server in which the cookie will be available on. If set to '/', the cookie will be available within the entire domain . If set to '/foo/', the cookie will only be available within the /foo/ directory and all sub-directories such as /foo/bar/ of domain . The default value is the current directory that the cookie is being set in."),
    '#default_value' => isset($cookie['path']) ? $cookie['path'] : '',
  );
  $form['domain'] = array(
    '#type'       => 'textfield',
    '#title'      => t('Cookie domain'),
    '#description'=> t("The domain that the cookie is available. To make the cookie available on all subdomains of example.com then you'd set it to '.example.com'. The . is not required but makes it compatible with more browsers. Setting it to www.example.com will make the cookie only available in the www subdomain."),
    '#default_value' => isset($cookie['domain']) ? $cookie['domain'] : '',
  );
  $form['secure'] = array(
    '#type'       => 'checkbox',
    '#title'      => t('Secure cookie'),
    '#description'=> t("Indicates that the cookie should only be transmitted over a secure HTTPS connection from the client. When set to TRUE, the cookie will only be set if a secure connection exists. The default is FALSE."),
    '#default_value' => isset($cookie['secure']) ? $cookie['secure'] : 0,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}
function logincookie_set_form_validate($form, &$form_state) {
  $cookie = $form_state['values'];
  $when = 'logincookie_'. $cookie['when'];
  $cookie = $form_state['values'];
  $logincookie_default = array(
    'set'   => array(),
    'unset' => array(),
  );
}
function logincookie_set_form_submit($form, &$form_state) {
  $cookie = $form_state['values'];
  $when = 'logincookie_'. $cookie['when'];
  $logincookie_default = array(
    'set'   => array(),
    'unset' => array(),
  );
  $cookies = variable_get('logincookie_'. $cookie['when'], $logincookie_default);
  if (isset($cookie['old-cookie']) )  {
    unset($cookies['set'][$cookie['old-cookie']['name']]);
  }
  $cookies['set'][$cookie['name']]['name'] = $cookie['name'];
  $cookies['set'][$cookie['name']]['expire'] = $cookie['expire'];
  $cookies['set'][$cookie['name']]['value'] = $cookie['value'];
  $cookies['set'][$cookie['name']]['path'] = $cookie['path'];
  $cookies['set'][$cookie['name']]['domain'] = $cookie['domain'];
  $cookies['set'][$cookie['name']]['secure'] = $cookie['secure'];

  variable_set('logincookie_'. $cookie['when'], $cookies);
//drupal_set_message('<pre>d'. print_r($cookie, 1 ) . '</pre>');
//drupal_set_message('<pre>d'. print_r($cookies, 1 ) . '</pre>');
  $form_state['redirect'] = array('admin/settings/logincookie');
}

function logincookie_unset_form($form_status, $cookie) {
  $form = array();
  if (isset($cookie)) {
    $form['old-cookie'] = array(
      '#type' => 'value',
      '#value' => $cookie,
    );
  }
  $form['when'] = array(
    '#type'     => 'select',
    '#title'    => t('Cookie action'),
    '#description' => t('Select <em>when</em> to set cookie. Ex: selecting "login" will set this cookie when user logs in the site.'),
    '#options'  => array(
      0 => 'Choose action',
      'login' => 'Login',
      'logout' => 'Logout',
    ),
    '#default_value' => isset($cookie['when']) ? $cookie['when'] : '0',
  );
  $form['name'] = array(
    '#type'       => 'textfield',
    '#title'      => t('Cookie Name'),
    '#description'=> t('The name of the cookie.'),
    '#default_value' => isset($cookie['name']) ? $cookie['name'] : '',
  );
  $form['path'] = array(
    '#type'       => 'textfield',
    '#title'      => t('Cookie path'),
    '#description'=> t("The path on the server in which the cookie will be available on. If set to '/', the cookie will be available within the entire domain . If set to '/foo/', the cookie will only be available within the /foo/ directory and all sub-directories such as /foo/bar/ of domain . The default value is the current directory that the cookie is being set in."),
    '#default_value' => isset($cookie['path']) ? $cookie['path'] : '',
  );
  $form['domain'] = array(
    '#type'       => 'textfield',
    '#title'      => t('Cookie domain'),
    '#description'=> t("The domain that the cookie is available. To make the cookie available on all subdomains of example.com then you'd set it to '.example.com'. The . is not required but makes it compatible with more browsers. Setting it to www.example.com will make the cookie only available in the www subdomain."),
    '#default_value' => isset($cookie['domain']) ? $cookie['domain'] : '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}
function logincookie_unset_form_validate($form, &$form_state) {


}
function logincookie_unset_form_submit($form, &$form_state) {
  $cookie = $form_state['values'];
  $when = 'logincookie_'. $cookie['when'];
  $logincookie_default = array(
    'set'   => array(),
    'unset' => array(),
  );
  $cookies = variable_get('logincookie_'. $cookie['when'], $logincookie_default);
  if (isset($cookie['old-cookie']) )  {
    unset($cookies['unset'][$cookie['old-cookie']['name']]);
  }
  $cookies['unset'][$cookie['name']]['name'] = $cookie['name'];
  $cookies['unset'][$cookie['name']]['path'] = $cookie['path'];
  $cookies['unset'][$cookie['name']]['domain'] = $cookie['domain'];

  variable_set('logincookie_'. $cookie['when'], $cookies);

  $form_state['redirect'] = array('admin/settings/logincookie');
}
function logincookie_settings_page() {
  $logincookie_default = array(
    'set'   => array(),
    'unset' => array(),
  );
  $supported_ops = array('login', 'logout');
  $header = array('Set/Unset', 'op', 'Name', 'Value', 'Expire', 'Path', 'Domain', 'Secure',);
  $output = '';
  foreach ($supported_ops as $op){
    $output .= $op;
    $rows = array();
    $cookies = variable_get('logincookie_'. $op, array(), $logincookie_default);
    foreach($cookies as $index => $set) {
      foreach( $set as $name => $cookie) {
        if ($index == 'unset') {
          $expire = 'time() - 3600';
        }
        else {
          $expire = 'time() + ' . $cookie['expire'];
        }
        $rows[] = array($index,
          l('edit','admin/settings/logincookie/'.$index .'/'. $cookie['name']. '/'.$op). ' - ' .
          l('delete','admin/settings/logincookie/delete/'.$index .'/'. $cookie['name']. '/'.$op)
          , $cookie['name'], $cookie['value'], $expire, $cookie['path'], $cookie['domain'], $cookie['secure']);
      }
    }
    $output .=  theme('table', $header, $rows);
  }
  return $output;
}


